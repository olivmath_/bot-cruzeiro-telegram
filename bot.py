import telebot
import json
import os



API_TOKEN = os.getenv("PROD")
bot = telebot.TeleBot(API_TOKEN, parse_mode="MARKDOWN")


def json_commands() -> dict:
    with open("commands.json", "r") as file:
        return json.load(file)

commands = list(json_commands().keys())


@bot.message_handler(commands=commands)
def send_welcome(message):
    commands_response = json_commands()
    command = message.text[1:]
    response = '\n'.join(
        commands_response.get(command, "Command not found")
    )
    bot.reply_to(
        message,
        response
    )

bot.polling()