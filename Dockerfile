FROM alpine:3.14

RUN apk --no-cache --update --upgrade add python3-dev py-pip
WORKDIR /home/

COPY bot.py .
COPY commands.json .

RUN pip install pyTelegramBotAPI

CMD [ "python3", "bot.py" ]
