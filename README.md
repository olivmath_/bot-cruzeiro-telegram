# Bot Telegram Cruzeiro


## Boas Vindas
- Quando entrar um novo membro:
    - enviar mensagem de boas vindas
    - verificar o novo membro com Captcha

## Comandos
- */lista*
    - */comprar*
    - */links*
    - */ajuda*
    - */lista*
- */compra*
    - *futtoken*
    - *wallet*
    - *pay*
- */links*
    - *Foottoken*
    - *LunesWallet*
    - *LunesPay*
- */ajuda*
    - *Telegram*
    - *Instagram*
    - *Facebook*
    - *Email*

## Links

*Canais de Venda*
- [futtoken](https://futtoken.com/)
- [wallet](https://luneswallet.app/)
- [pay](https://play.google.com/store/apps/details?id=com.lunespay&hl=pt_BR&gl=US/)

*link*
- [facebook](https://www.facebook.com/cruzeirooficial)
- [instagram](https://www.instagram.com/cruzeiro/?hl=pt-br)
- [twitter](https://twitter.com/cruzeiro)
- [youtube](https://www.youtube.com/c/OficialCruzeiro/videos)
- [tiktok](https://www.tiktok.com/@cruzeiro)

*ajuda*
- [telegram](https://t.me/CruzeiroFanToken)
- [facebook](https://www.facebook.com/cruzeirooficial)
- [instagram](https://www.instagram.com/cruzeiro/?hl=pt-br)
- [email]()

## Notícias
- Post periódico coletado do [telegram news]()